using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Record
{
    public float energy;

    public float digestBacteriaAbility;
    public float digestCarbonAbility;
    public float digestOxygenAbility;

    public float eatOxygenProbability;
    public float eatCarbonProbability;
    public float eatBacteriaProbability;

    public Record(Record record)
    {
        digestOxygenAbility = record.digestOxygenAbility;
        digestCarbonAbility = record.digestCarbonAbility;
        digestBacteriaAbility = record.digestBacteriaAbility;

        eatOxygenProbability = record.eatOxygenProbability;
        eatCarbonProbability = record.eatCarbonProbability;
        eatBacteriaProbability = record.eatBacteriaProbability;

        energy = record.energy;
    }

    public Record(Bacteria bacteria)
    {
        digestOxygenAbility = bacteria.digestOxygenAbility;
        digestCarbonAbility = bacteria.digestCarbonAbility;
        digestBacteriaAbility = bacteria.digestBacteriaAbility;

        eatOxygenProbability = bacteria.eatOxygenProbability;
        eatCarbonProbability = bacteria.eatCarbonProbability;
        eatBacteriaProbability = bacteria.eatBacteriaProbability;

        energy = bacteria.energy;
    }
}
