using UnityEngine;

public class Bacteria : MonoBehaviour
{
    public Simulation simulation;

    public float digestOxygenAbility;
    public float digestCarbonAbility;
    public float digestBacteriaAbility;

    public float eatOxygenProbability;
    public float eatCarbonProbability;
    public float eatBacteriaProbability;

    public float energy = 0f;
    public bool eaten = false;

    public bool generationEnded = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ConsiderEating(Food.Type type)
    {
        switch (type)
        {
            case Food.Type.Oxygen:
                {
                    if (Random.Range(0f, 1f) < eatOxygenProbability)
                    {
                        energy += digestOxygenAbility;
                        simulation.AddCarbons(2);
                        eaten = true;
                    }
                    return;
                }
            case Food.Type.Carbon:
                {
                    if (Random.Range(0f, 1f) < eatCarbonProbability)
                    {
                        energy += digestCarbonAbility;
                        eaten = true;
                    }
                    return;
                }
            case Food.Type.Bacteria:
                {
                    if (Random.Range(0f, 1f) < eatBacteriaProbability)
                    {
                        energy += digestBacteriaAbility;
                        eaten = true;
                    }
                    return;
                }
        }
    }
}
