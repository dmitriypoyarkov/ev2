import json
import glob
from bokeh.plotting import figure, output_file, show
from bokeh.models import ColumnDataSource


DATA_FOLDER = 'A:\\UnityProj\\Ev2\\Assets\Data\\16.04.2022 12-47-59\\'

def main():
    gen_num = 10
    data = {
        'gen' : [],
        'oxygen_eaters' : [],
        'carbon_eaters' : [],
        'bacteria_eaters' : []
    }
    while True:
        try:
            with open(DATA_FOLDER + 'gen' + str(gen_num) + '.json', 'r') as data_file:
                gen_data = json.load(data_file)
        except OSError:
            break
        items = gen_data['Items']
        item = items[0]['digestOxygenAbility']
        oxygen_eaters = [item['digestOxygenAbility'] for item in items
            if item['digestOxygenAbility'] > 0.75]
        carbon_eaters = [item['digestCarbonAbility'] for item in items
            if item['digestCarbonAbility'] > 0.75]
        bacteria_eaters = [item['digestBacteriaAbility'] for item in items
            if item['digestBacteriaAbility'] > 0.75]
        data['gen'].append(gen_num)
        data['oxygen_eaters'].append(len(oxygen_eaters))
        data['carbon_eaters'].append(len(carbon_eaters))
        data['bacteria_eaters'].append(len(bacteria_eaters))
        gen_num += 10
    source = ColumnDataSource(data=data)
    fig=figure()
    fig.line(x='gen', y='oxygen_eaters', source=source, color='green')
    fig.line(x='gen', y='carbon_eaters', source=source, color='blue')
    fig.line(x='gen', y='bacteria_eaters', source=source, color='red')
    show(fig)
    

    

if __name__ == '__main__':
    main()