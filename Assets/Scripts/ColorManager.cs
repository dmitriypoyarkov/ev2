using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    [SerializeField] Bacteria entity;
    [SerializeField] SpriteRenderer sprite;
    void Start()
    {
        sprite.color = new Color(
            1.5f * entity.digestBacteriaAbility,
            1.5f * entity.digestOxygenAbility,
            1.5f * entity.digestCarbonAbility,
            1f
        );
    }
}
