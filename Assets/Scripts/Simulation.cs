using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Simulation : MonoBehaviour
{
    public string startGenerationFile = "";
    public string startGenerationPath = "";
    public int startArchiveCount = 0;
    public SpriteRenderer foodField;
    public SpriteRenderer bacteriaField;
    public RecordHolder recordHolder;
    public GameObject oxygenGo;
    public GameObject carbonGo;
    public GameObject bacteriaGo;
    public Transform bacteriaHighlighter;
    public Transform foodHighlighter;
    public int simulationSteps;
    public float mutationRate;
    public int foodCount;
    public int foodX;
    public int foodY;
    public int bacteriaCount;
    public int bacteriaX;
    public int bacteriaY;

    public float carbonsProbabilityWeight = 3f;
    public float bacteriesProbabilityWeight = 0.04f;

    public Dictionary<Vector2, Food> foodGrid = new Dictionary<Vector2, Food>();
    public Dictionary<Vector2, Bacteria> bacteriaGrid = new Dictionary<Vector2, Bacteria>();

    public List<Food> Foods
    {
        get
        {
            return foodGrid
                .Where(x => x.Value != null && x.Value.gameObject.activeSelf)
                .Select(x => x.Value)
                .Concat(bacteriaGrid
                            .Where(x => x.Value)
                            .Select(x => x.Value.GetComponent<Food>())
                        )
                .ToList();
        }
    }

    public void Start()
    {
        if (startGenerationFile != "")
        {
            recordHolder.dirPath = startGenerationPath;
            recordHolder.archiveCount = startArchiveCount;
            var fileContent = System.IO.File.ReadAllText(startGenerationFile);

            var startGenerationRecords = JsonHelper.FromJson<Record>(fileContent).ToList();
            StartCoroutine(Simulate(startGenerationRecords));
            //GenerateBacteria(startGenerationRecords);
        }
        else
        {
            GenerateOxygen();
            GenerateBacteria();
            StartCoroutine(Simulate());
        }
    }

    public IEnumerator Simulate(List<Record> startRecords = null)
    {
        // Select and mutate
        if (startRecords != null)
        {
            var randm = new System.Random();
            randm.Shuffle(startRecords);
            var newGeneration = new List<Record>();
            Record winner;
            for (int i = 0; i < startRecords.Count - 1; i += 2)
            {
                winner = startRecords[i].energy > startRecords[i + 1].energy ?
                    startRecords[i] : startRecords[i + 1];
                newGeneration.Add(winner);
                newGeneration.Add(Mutate(winner));
            }
            GenerateBacteria(newGeneration);
            GenerateOxygen();
        }
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < simulationSteps; i++)
        {
            foreach (var bacteria in bacteriaGrid.Values)
            {
                if (bacteria == null || bacteria.eaten || !bacteria.gameObject.activeSelf)
                {
                    continue;
                }
                yield return new WaitForSeconds(1f);
                var randomFood = SelectRandomFood(bacteria.GetComponent<Food>());
                Highlight(bacteria, randomFood);
                yield return new WaitForSeconds(1f);
                bacteria.ConsiderEating(randomFood.type);
                if (bacteria.eaten)
                {
                    if (randomFood.type == Food.Type.Bacteria)
                    {
                        recordHolder.AddRecord(
                            new Record(randomFood.GetComponent<Bacteria>())
                        );
                    }
                    randomFood.gameObject.SetActive(false);
                }
                yield return new WaitForSeconds(1f);
                DeHighlight();
            }
            yield return new WaitForSeconds(1f);
            var unEatenBacteries = bacteriaGrid.Values.Where(f => f && !f.eaten).ToList();
            if (unEatenBacteries.Count == 0)    
            {
                break;
            }
        }
        // Generation end; Record generation
        yield return new WaitForSeconds(1f);
        foreach (var bacteria in bacteriaGrid.Values)
        {
            if (bacteria != null)
            {
                bacteria.generationEnded = true;
                recordHolder.AddRecord(new Record(bacteria));
                Destroy(bacteria.gameObject);
            }
        }
        recordHolder.MoveRecordsToArchive();

        foreach (var food in foodGrid.Values)
        {
            if (food != null)
            {
                Destroy(food.gameObject);
            }
        }
        foodGrid.Clear();
        StartCoroutine(Simulate(recordHolder.archive.Last()));
    }

    public Food SelectRandomFood(Food excluded)
    {
        var foods = Foods.Except(new List<Food>() { excluded });
        var oxygens = foods.Where(f => f.type == Food.Type.Oxygen).ToList();
        var carbons = foods.Where(f => f.type == Food.Type.Carbon).ToList();
        var bacteries = foods.Where(f => f.type == Food.Type.Bacteria).ToList();

        var oxygensWeightedLen = oxygens.Count;
        var carbonsWeightedLen = carbons.Count * carbonsProbabilityWeight;
        var bacteriesWeightedLen = bacteries.Count * bacteriesProbabilityWeight;

        var weightedLenSum = oxygensWeightedLen + carbonsWeightedLen
            + bacteriesWeightedLen;
        if (weightedLenSum == 0)
        {
            return null;
        }
        var oxygenProbability = oxygensWeightedLen / weightedLenSum;
        var carbonProbability = carbonsWeightedLen / weightedLenSum;
        var random01 = Random.Range(0f, 1f);
        if (random01 < oxygenProbability)
        {
            return oxygens[Random.Range(0, oxygens.Count)];
        }
        if (random01 < carbonProbability)
        {
            return carbons[Random.Range(0, carbons.Count)];
        }
        return bacteries[Random.Range(0, bacteries.Count)];
    }

    public void Highlight(Bacteria bacteria, Food food)
    {
        foodHighlighter.gameObject.SetActive(true);
        foodHighlighter.position = food.transform.position;
        bacteriaHighlighter.gameObject.SetActive(true);
        bacteriaHighlighter.position = bacteria.transform.position;
    }

    public void DeHighlight()
    {
        foodHighlighter.gameObject.SetActive(false);
        bacteriaHighlighter.gameObject.SetActive(false);
    }

    public void GenerateOxygen()
    {
        var gridSize = new Vector2(foodField.bounds.size.x / foodX, foodField.bounds.size.y / foodY);
        for (int i = 0; i < foodX; i++)
        {
            for (int j = 0; j < foodY; j++)
            {
                var position = new Vector2(
                        foodField.bounds.min.x + (i + 0.5f) * gridSize.x,
                        foodField.bounds.min.y + (j + 0.5f) * gridSize.y
                    );
                if (foodGrid.Count >= foodCount)
                {
                    foodGrid.Add(position, null);
                    continue;
                }
                var oxygen = Instantiate(oxygenGo, position, Quaternion.identity, transform)
                    .GetComponent<Food>();
                foodGrid.Add(
                    position, oxygen
                );
            }
        }
    }

    public void GenerateBacteria(List<Record> records = null)
    {
        var gridSize = new Vector2(bacteriaField.bounds.size.x / foodX,
            bacteriaField.bounds.size.y / foodY);
        bacteriaGrid.Clear();

        for (int i = 0; i < foodX; i++)
        {
            for (int j = 0; j < foodY; j++)
            {
                var position = new Vector2(
                    bacteriaField.bounds.min.x + (i + 0.5f) * gridSize.x,
                    bacteriaField.bounds.min.y + (j + 0.5f) * gridSize.y
                );
                if (bacteriaGrid.Count >= bacteriaCount)
                {
                    bacteriaGrid.Add(position, null);
                    continue;
                }
                var bacteria = Instantiate(bacteriaGo, position, Quaternion.identity, transform)
                    .GetComponent<Bacteria>();
                bacteria.simulation = this;
                bacteriaGrid.Add(
                    position,
                    bacteria
                );

                if (records is null)
                {
                    bacteria.digestOxygenAbility = Random.Range(0f, 1f);
                    bacteria.digestCarbonAbility = Random.Range(0f, 1f);
                    bacteria.digestBacteriaAbility = Random.Range(0f, 1f);

                    bacteria.eatOxygenProbability = Random.Range(0f, 1f);
                    bacteria.eatCarbonProbability = Random.Range(0f, 1f);
                    bacteria.eatBacteriaProbability = Random.Range(0f, 1f);
                }
                else
                {
                    bacteria.digestOxygenAbility = records[bacteriaGrid.Count].digestOxygenAbility;
                    bacteria.digestCarbonAbility = records[bacteriaGrid.Count].digestCarbonAbility;
                    bacteria.digestBacteriaAbility = records[bacteriaGrid.Count].digestBacteriaAbility;
                    
                    bacteria.eatOxygenProbability = records[bacteriaGrid.Count].eatOxygenProbability;
                    bacteria.eatCarbonProbability = records[bacteriaGrid.Count].eatCarbonProbability;
                    bacteria.eatBacteriaProbability = records[bacteriaGrid.Count].eatBacteriaProbability;
                }
                var digestSum = bacteria.digestOxygenAbility +
                    bacteria.digestCarbonAbility +
                    bacteria.digestBacteriaAbility + 0.001f; //0.001 for non-zero
                bacteria.digestOxygenAbility /= digestSum;
                bacteria.digestCarbonAbility /= digestSum;
                bacteria.digestBacteriaAbility /= digestSum;
            }
        }
        return;
    }

    Record Mutate(Record bacteria)
    {
        var mutated = new Record(bacteria);
        mutated.digestOxygenAbility = Mathf.Clamp01(
                        mutated.digestOxygenAbility +
                        Gaussian() * mutationRate);
        mutated.digestCarbonAbility = Mathf.Clamp01(
            mutated.digestCarbonAbility +
            Gaussian() * mutationRate);
        mutated.digestBacteriaAbility = Mathf.Clamp01(
            mutated.digestBacteriaAbility +
            Gaussian() * mutationRate);

        mutated.eatOxygenProbability = Mathf.Clamp01(
            bacteria.eatOxygenProbability + Gaussian() * mutationRate);
        mutated.eatCarbonProbability = Mathf.Clamp01(
            bacteria.eatCarbonProbability + Gaussian() * mutationRate);
        mutated.eatBacteriaProbability = Mathf.Clamp01(
            bacteria.eatBacteriaProbability + Gaussian() * mutationRate);
        return mutated;
    }

    float Gaussian()
    {
        float sigma = 1f;
        float x = Random.Range(-8f, 8f);

        int randomSign = Random.Range(0, 2) == 0 ? -1 : 1;
        return randomSign / (sigma * Mathf.Sqrt(2 * Mathf.PI)) *
            Mathf.Exp(-0.5f * Mathf.Pow(x / sigma, 2));
    }

    public void AddCarbons(int amount)
    {
        var foodPositions = foodGrid.Keys.ToList();
        foreach (var foodPosition in foodPositions)
        {
            if (foodGrid[foodPosition] == null || foodGrid[foodPosition].gameObject.activeSelf == false)
            {
                if (foodGrid[foodPosition] != null)
                {
                    Destroy(foodGrid[foodPosition].gameObject);
                }
                var carbon = Instantiate(carbonGo, foodPosition, Quaternion.identity, transform)
                    .GetComponent<Food>();

                foodGrid[foodPosition] = carbon;
                if (--amount == 0)
                {
                    break;
                }
            }
        }
    }
}
